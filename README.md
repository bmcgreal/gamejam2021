# gamejam2021
GitHub Game Jam for November 2021

# Notes
For object interactions to work correctly, the main scene needs a player scene named "player". This player scene needs to have a node with the path "Sprites/HandHolder" which will act as the anchor for anything the player holds.