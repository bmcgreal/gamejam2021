extends KinematicBody2D

# movement variables
var velocity = Vector2(0, 0)
var speed = 170
export var frontVector = Vector2(0, 0)
var animationPlayerFrameLength = 0.3/3

# some state variables to decide what animation to play
var pressedUp = false
var pressedDown = false
var pressedLeft = false
var pressedRight = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_movement_input():
	velocity = Vector2()
	if Input.is_action_pressed("down"):
		velocity.y += 1
		pressedDown = true
	if Input.is_action_pressed("up"):
		velocity.y -= 1
		pressedUp = true
	if Input.is_action_pressed("left"):
		velocity.x -= 1
		pressedLeft = true
	if Input.is_action_pressed("right"):
		velocity.x += 1
		pressedRight = true
	# normalize
	velocity = velocity.normalized() * speed

func select_animation():
	# use logic to determine which animation to play
	if pressedDown:
		# if down is pressed always play the down animation regardless of what else is pressed
		$Sprites/AnimationPlayer.current_animation = "WalkDown"
		frontVector = Vector2(0, 1)
	elif pressedUp:
		# if up is pressed, play the up animation unless down is pressed
		$Sprites/AnimationPlayer.current_animation = "WalkUp"
		frontVector = Vector2(0,-1)
	elif pressedLeft:
		# play left animation
		$Sprites/AnimationPlayer.current_animation = "WalkLeft"
		frontVector = Vector2(-1,0)
	elif pressedRight:
		# play right animation
		$Sprites/AnimationPlayer.current_animation = "WalkRight"
		frontVector = Vector2( 1,0)
	else:
		# check if an animation is playing:
		if $Sprites/AnimationPlayer.is_playing():
			# stop the animation
			$Sprites/AnimationPlayer.stop(false)
			# pause for one animation frame
			yield(get_tree().create_timer(animationPlayerFrameLength), "timeout")
			# force to idle state (animation index 1)
			$Sprites/AnimationPlayer.seek(animationPlayerFrameLength*1, true)
			
	reset_input_bools()

func reset_input_bools():
	pressedDown = false
	pressedUp = false
	pressedLeft = false
	pressedRight = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	get_movement_input()
	velocity = move_and_slide(velocity)
	select_animation()

func _on_AnimationPlayer_animation_started(anim_name):
	# if walking right, the sprites need to be horizontally flipped
	if anim_name == "WalkRight":
		$Sprites/Body.flip_h = true
	else:
		$Sprites/Body.flip_h = false
		
