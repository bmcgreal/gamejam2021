extends "res://entities/holdable_item.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	# initialize some variables
	sprite = $Sprite
	centerLocation = Vector2(0, 0)
	thisScene = load("res://entities/coffee.tscn")
	highlightThickness = 0.06
	# set up shader since sprite is initialized
	self.configure_shader()

func _on_Area2D_mouse_entered():
	mouseHovered = true

func _on_Area2D_mouse_exited():
	mouseHovered = false
