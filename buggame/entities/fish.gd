extends Node2D

var player
var interactRange = 100
export var isHeld = false
var mouseHovered = false
var isHighlighted = false
var thisScene = load("res://entities/fish.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	# hide the highlighted sprite
	# reference: https://www.reddit.com/r/godot/comments/84fhuh/2d_outline_shader_finally_got_it_working/
	# I can't believe this random reddit code works so well LMAO
	$Sprite.hide()
	$Sprite_no_outline.show()
	# get reference to the player
	player = get_parent().get_tree().get_root().find_node("player", true, false)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# check if being held in the player's hand
	if not isHeld:
		# since we're not in hand, check if the mouse is hovered
		if mouseHovered:
			# check if the player is close enough to interact
			if get_distance_to_player() <= interactRange:
				# if not already highlighted
				if not isHighlighted:
					# highlight it
					$Sprite.show()
					$Sprite_no_outline.hide()
					isHighlighted = true
			else:
				# un-highlight it
				$Sprite.hide()
				$Sprite_no_outline.show()
				isHighlighted = false
		else:
			if isHighlighted:
				# un-highlight it
				$Sprite.hide()
				$Sprite_no_outline.show()
				isHighlighted = false

func get_distance_to_player():
	# get the distance to the node named "player"
	#var distance = player.get_global_position().distance_to(get_viewport().get_mouse_position())
	var distance = player.get_global_position().distance_to(get_global_position())
	return distance

func on_player_interact():
	# instance another fish scene in the hand of the player
	var inHandScene = thisScene.instance()
	inHandScene.scale = self.scale / player.scale / 1.5
	inHandScene.isHeld = true
	player.get_node("Sprites/HandHolder").add_child(inHandScene)
	# delete the current scene
	self.queue_free()

func drop_item():
	# instance another fish scene in the global scene
	var globalScene = thisScene.instance()
	globalScene.scale = self.scale * player.scale * 1.5
	globalScene.isHeld = false
	globalScene.position = player.position + 40 * player.frontVector
	get_parent().get_tree().get_root().add_child(globalScene)
	# delete the current scene
	self.queue_free()

func check_player_hand_full():
	# check if the player's "HandHolder" node has any children
	# is so, return true, otherwise return false
	if player.get_node("Sprites/HandHolder").get_child_count() == 0:
		return false
	else:
		return true

func _input(event):
	# check if interact was pressed
	if event.is_action_pressed("interact"):
		# check if interaction is ok (highlighted means it's ok)
		if isHighlighted:
			# check if the player is already holding something
			if not check_player_hand_full():
				# interact with the object (pick it up)
				on_player_interact()
				get_tree().set_input_as_handled()
	# check if drop was pressed
	elif event.is_action_pressed("drop_item"):
		if isHeld:
			# drop the item
			drop_item()

func _on_Area2D_mouse_entered():
	mouseHovered = true

func _on_Area2D_mouse_exited():
	mouseHovered = false
