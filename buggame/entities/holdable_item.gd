# This script establishes the basic functionality of a holdable item
# Specific items should extend this script, but customize the sprites etc.
# NOTE: each child script needs to handle mouse hovering monitoring on its own,
# since that involves connecting signals
# 11/15/21

extends Node2D

# Script variables

# These variables need to be given a value by child scripts:
var centerLocation
var thisScene
var sprite
# These variables do not need to given a value by child scripts (but they can be)
var player
var playerHand
var interactRange = 60
var mouseHovered = false
var isHighlighted = false
export var isHeld = false
var inHandDownScale = 1.5
var highlightThickness = 0.015
var outlineColor = Plane( 1, 0.8, 0, 1 )
const highlightShader = preload("res://misc/outline.shader")

# Called when the node enters the scene tree for the first time.
func _ready():
	# get reference to the player and player hand
	player = get_parent().get_tree().get_root().find_node("player", true, false)
	playerHand = player.get_node("Sprites/HandHolder")

func configure_shader():
	# this should be done in "_ready()" but sprite needs to be initialized first
	# set up item shader
	var mat = ShaderMaterial.new()
	mat.set_shader(highlightShader)
	mat.set_shader_param("outLineSize", 0)
	mat.set_shader_param("outLineColor", outlineColor)
	sprite.set_material(mat)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# if item is in player's hand, do nothing
	if not isHeld:
		# since we're not in hand, check if the mouse is hovered
		if mouseHovered:
			# check if the player is close enough to interact
			if get_distance_to_player() <= interactRange:
				# if not already highlighted
				if not isHighlighted:
					# highlight it
					set_highlight(true)
			else:
				# un-highlight it
				set_highlight(false)
		else:
			# mouse is not hovered, so item should not be highlighted
			if isHighlighted:
				# un-highlight it
				set_highlight(false)

# Highlighting function
func set_highlight(state):
	# turns the item highlighting on or off
	# if state = true, the item will be highlighted
	# if state = false, the item will not be highlighted
	if state == true:
		sprite.get_material().set_shader_param("outLineSize", highlightThickness)
	else:
		sprite.get_material().set_shader_param("outLineSize", 0)
	isHighlighted = state

# input handler
func _input(event):
	# if interact was pressed
	if event.is_action_pressed("interact"):
		# check if interaction is ok (highlighted means it's ok)
		if isHighlighted:
			# check if the player is already holding something
			if not check_player_hand_full():
				# interact with the object (pick it up)
				player_interact()
				# set the input as handled
				get_tree().set_input_as_handled()
	# check if drop was pressed
	elif event.is_action_pressed("drop_item"):
		if isHeld:
			# drop the item
			drop_item()

# input helper functions
func player_interact():
	# instance another scene in the hand of the player
	var inHandScene = thisScene.instance()
	inHandScene.scale = self.scale / player.scale / inHandDownScale
	inHandScene.isHeld = true
	playerHand.add_child(inHandScene)
	# delete the current scene
	self.queue_free()

func drop_item():
	# instance another scene in the global scene
	var globalScene = thisScene.instance()
	globalScene.scale = self.scale * player.scale * inHandDownScale
	globalScene.isHeld = false
	globalScene.position = player.position + 40 * player.frontVector
	get_parent().get_tree().get_root().add_child(globalScene)
	# delete the current scene
	self.queue_free()

# MISC helper functions:
func get_distance_to_player():
	# get the distance to the node named "player"
	var itemCenter = get_global_position() + (centerLocation*self.scale).rotated(self.rotation)
	var distance = player.get_global_position().distance_to(itemCenter)
	return distance

func check_player_hand_full():
	# check if the player's "HandHolder" node has any children
	# is so, return true, otherwise return false
	if playerHand.get_child_count() == 0:
		return false
	else:
		return true
