extends Node2D

# state variables
var isOpen = false
var mouseHovered = false
var interactRange = 150
var centerVec = Vector2(0,0)
var outlineColor = Plane( 1, 0.8, 0, 1 )
var isHighlighted = false
var highlightThickness = 0.015
# references to scene children
var sprite
var areaTwoD
var collShape
# reference to the player
var player
# preload the sprite textures
var fridgeOpen = preload("res://entities/fridge_open.PNG")
var fridgeClosed = preload("res://entities/fridge_closed.PNG")
const SHADER = preload("res://misc/outline.shader")
# signal for when the fridge is opened
signal fridge_opened

# Called when the node enters the scene tree for the first time.
func _ready():
	# get references to children
	sprite = get_node("Sprite")
	areaTwoD = get_node("Area2D")
	collShape = get_node("Area2D/CollisionShape2D")
	# call the "change_state" method to initialize the texture
	if isOpen:
		change_state("open")
	else:
		change_state("closed")
	# get reference to the player
	player = get_parent().get_tree().get_root().find_node("player", true, false)
	# calculate offset to center of fridge
	centerVec = Vector2(sprite.texture.get_size())/2

func change_state(state):
	# change sprite image
	if state == "open":
		sprite.texture = fridgeOpen
		sprite.z_index = 0
		emit_signal("fridge_opened")
	elif state == "closed":
		sprite.texture = fridgeClosed
		sprite.z_index = 1
	# set sprite material
	var mat = ShaderMaterial.new()
	mat.set_shader(SHADER)
	mat.set_shader_param("outLineSize", highlightThickness)
	mat.set_shader_param("outLineColor", outlineColor)
	sprite.set_material(mat)
	isHighlighted = true
	# get sprite size
	var spriteTextureSize = sprite.texture.get_size()
	# move sprite to have top left corner at the origin
	sprite.position.x = spriteTextureSize.x/2
	sprite.position.y = spriteTextureSize.y/2
	# adjust collision shape to cover the whole texture
	collShape.position.x = spriteTextureSize.x/2
	collShape.position.y = spriteTextureSize.y/2
	collShape.shape.set_extents(Vector2(spriteTextureSize.x/2, spriteTextureSize.y/2))

func get_distance_to_player():
	# get the distance to the node named "player"
	var fridgeCenter = get_global_position() + (centerVec*self.scale).rotated(self.rotation)
	var distance = player.get_global_position().distance_to(fridgeCenter)
	return distance

func _input(event):
	# check if interact was pressed
	if event.is_action_pressed("interact"):
		# check if the mouse if hovered
		if mouseHovered:
			# check if the player is close enough to interact
			if get_distance_to_player() <= interactRange:
				if isOpen:
					change_state("closed")
					isOpen = false
				else:
					change_state("open")
					isOpen = true
				get_tree().set_input_as_handled()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# check if the mouse is hovered
	if mouseHovered:
		# check if the player is close enough to interact
		if get_distance_to_player() <= interactRange:
			# if not already highlighted
			if not isHighlighted:
				# highlight it
				sprite.get_material().set_shader_param("outLineSize", highlightThickness)
				isHighlighted = true
		else:
			# un-highlight it
			sprite.get_material().set_shader_param("outLineSize", 0)
			isHighlighted = false
	else:
		if isHighlighted:
			# un-highlight it
			sprite.get_material().set_shader_param("outLineSize", 0)
			isHighlighted = false

func _on_Area2D_mouse_entered():
	# mouse is over the shape
	mouseHovered = true

func _on_Area2D_mouse_exited():
	# mouse is no longer over the shape
	mouseHovered = false
