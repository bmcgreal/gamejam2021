extends KinematicBody2D

#stole this straight up from godot docs
#set up so that it it always facing the mouse cursor
export (int) var speed = 230

var velocity = Vector2()
var turningPenalty = 0.15
var maxTurnRate = 0.1 # rad/frame

func get_movement_input():
	var distanceToMouse = get_global_mouse_position().distance_to(get_position())
	if distanceToMouse > 20:
		rotate(min(maxTurnRate, turningPenalty * get_angle_to(get_global_mouse_position())))
		#look_at(get_global_mouse_position())
		velocity = Vector2(0,0)
		if Input.is_action_pressed("down"):
			velocity += Vector2(-speed, 0).rotated(rotation)
		if Input.is_action_pressed("up"):
			velocity += Vector2(speed, 0).rotated(rotation)
		if Input.is_action_pressed("right"):
			velocity += Vector2(0, speed).rotated(rotation)
		if Input.is_action_pressed("left"):
			velocity += Vector2(0, -speed).rotated(rotation)

func get_interaction_input():
	if Input.is_action_just_pressed("interact"):
		print("interaction pressed")
	

func _physics_process(delta):
	get_movement_input()
	velocity = move_and_slide(velocity)
	get_interaction_input()
